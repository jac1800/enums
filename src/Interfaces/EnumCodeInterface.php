<?php
/**
 * @Author: Jackywu
 * @Email: 403226004@qq.com
 * @Time: 2023/12/11 23:32
 */

namespace Jac1800\Enums\Interfaces;

interface EnumCodeInterface
{
    /**
     * @return string|null
     */
    public function getEnumMsg(): ?string;

    /**
     * @return int|null
     */
    public function getCode(): ?string;

}