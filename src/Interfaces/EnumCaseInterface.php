<?php
/**
 * @Author: Jackywu
 * @Email: 403226004@qq.com
 * @Time: 2023/12/11 23:29
 */

namespace Jac1800\Enums\Interfaces;

interface EnumCaseInterface
{
    /**
     * 获取拓展
     * @param $key
     * @return mixed
     */
    public function ext($key = null);

    /**
     * msg
     * @return string|null
     */
    public function msg(): ?string;

    /**
     * @return mixed
     */
    public function name(): mixed;

    /**
     * @param string|int $groupName
     * @param string|int|null $name
     * @return array|static|null
     */
    public static function group(string|int $groupName, string|int $name = null): array|static|null;

    /**
     * @return array
     */
    public function toArray(): array;

}