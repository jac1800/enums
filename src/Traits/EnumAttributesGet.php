<?php
/**
 * @Author: Jackywu
 * @Email: 403226004@qq.com
 * @Time: 2023/12/12 10:39
 */

namespace Jac1800\Enums\Traits;

use ReflectionEnum;
use ReflectionEnumUnitCase;

trait EnumAttributesGet
{
    /**
     * @param string $attribute
     * @return array
     * @throws \ReflectionException
     */
    public function getEnumAttributes(string $attribute): array
    {
        return array_map(
            fn($reflectionAttribute) => $reflectionAttribute->newInstance(),
            (new ReflectionEnum($this))
                ->getAttributes($attribute)
        );

    }

    /**
     * @param string $attribute
     * @param int $get
     * @return object|null
     * @throws \ReflectionException
     */
    public function getEnumAttribute(string $attribute, int $get = 0): ?object
    {
        return ((new ReflectionEnum($this))
            ->getAttributes($attribute)[$get] ?? null)
            ?->newInstance();
    }

    /**
     * @param string $attribute
     * @return array
     */
    public function getEnumCaseAttributes(string $attribute): array
    {
        return array_map(
            fn($reflectionAttribute) => $reflectionAttribute->newInstance(),
            (new ReflectionEnumUnitCase($this, $this->name))
                ->getAttributes($attribute)
        );
    }

    /**
     * @param string $attribute
     * @param int $get
     * @return object|null
     */
    public function getEnumCaseAttribute(string $attribute, int $get = 0): ?object
    {
        return ((new ReflectionEnumUnitCase($this, $this->name))
            ->getAttributes($attribute)[$get] ?? null)
            ?->newInstance();
    }
}