<?php
/**
 * @Author: Jackywu
 * @Email: 403226004@qq.com
 * @Time: 2023/12/11 23:38
 */

namespace Jac1800\Enums\Traits;

use ReflectionEnum;
use Jac1800\Enums\Annotations\EnumCase;
use Jac1800\Enums\Utils\EnumGroups;


trait EnumCaseGet
{
     use EnumAttributesGet;

    /**
     * 获取枚举解释
     * @param int $get
     * @return EnumCase|null
     */
    protected function getEnumCase(int $get = 0): ?EnumCase
    {
        return $this->getEnumCaseAttribute(EnumCase::class, $get);
    }

    /**
     * 获取拓展
     * @param $key
     * @return mixed|null
     */
    public function ext($key = null)
    {
        if (!empty($key)) {
            return $this->getEnumCase()->ext[$key] ?? null;
        }
        return $this->getEnumCase()->ext;
    }


    /**
     * 将枚举转换为数组
     * @return array
     */
    public function toArray(): array
    {
        return [
            'name'  => $this->name(),
            'value' => $this->value ?? null,
            'msg'   => $this->msg(),
            'group' => $this->getEnumCase()->group,
            'ext'   => $this->ext(),
        ];
    }

    /**
     * 解释
     * @return string|null
     */
    public function msg(): ?string
    {
        return $this->getEnumCase()?->msg;
    }

    /**
     * @return string|null
     */
    public function name(): ?string
    {
        return $this->getEnumCase()?->name ?? $this->name;
    }

    /**
     * 加载分组数据
     * @param object|string $enum
     * @return array
     * @throws \ReflectionException
     */
    protected static function loadGroups(object|string $enum): array
    {
        $enum = new ReflectionEnum($enum);
        if (EnumGroups::issetGroups($enum->getName())) {
            return EnumGroups::getGroups($enum->getName());
        }
        $enumCases = $enum->getCases();
        foreach ($enumCases as $enumCase) {
            /** @var self $case */
            $case = $enumCase->getValue();
            EnumGroups::setGroups($enum->getName(), $case->getEnumCase()?->group, $case->name(), $case);
        }

        return EnumGroups::getGroups($enum->getName());
    }

    /**
     * 获取分组数据
     * @param string|int $groupName
     * @param string|int|null $name
     * @return array|static|null
     * @throws \ReflectionException
     */
    public static function group(string|int $groupName, string|int $name = null): array|static|null
    {
        $groups = self::loadGroups(static::class);
        if ($name !== null) {
            return self::group($groupName)[$name] ?? null;
        }

        return $groups[$groupName] ?? null;
    }
    /**
     * @param string $name
     * @param array $arguments
     * @return mixed|null
     */
    public function __call(string $name, array $arguments)
    {
        $ext = $this->ext();
        $pos = stripos($name, 'get');
        if ($pos === 0) {
            $getKey = substr($name, 3);

            if (isset($ext[$getKey])) {
                return $ext[$getKey];
            }
        }
        if (isset($ext[$name])) {
            return $ext[$name];
        }

        return null;
    }
}