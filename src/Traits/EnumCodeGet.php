<?php
/**
 * @Author: Jackywu
 * @Email: 403226004@qq.com
 * @Time: 2023/12/12 11:02
 */

namespace Jac1800\Enums\Traits;

use Jac1800\Enums\Annotations\EnumCodePrefix;
use Jac1800\Enums\Annotations\EnumCode;
trait EnumCodeGet
{
    use EnumAttributesGet;

    /**
     * 获取枚举解释
     * @param int $get
     * @return EnumCodePrefix|null
     * @throws \ReflectionException
     */
     protected function getEnumCodePrefix(int $get = 0): ?EnumCodePrefix
     {
         return $this->getEnumAttribute(EnumCodePrefix::class, $get);
     }

    /**
     * 获取枚举解释
     * @param int $get
     * @return EnumCode|null
     */
      protected function getEnumCode(int $get = 0): ?EnumCode
      {
            return $this->getEnumCaseAttribute(EnumCode::class, $get);
      }

     /**
     * 获取错误信息
     * @return string
     */
     public function getEnumMsg(): string
     {
         return $this->getEnumCode()?->msg ?? '';
     }

     /**
     * 获取错误码
     * @return string|null
     * @throws \ReflectionException
     */
     public function getCode(): ?string
     {
          $code   = $this->value;
          $pfCode = $this->getEnumCodePrefix()?->pfCode;
          if ($pfCode === null || $pfCode === "") {
                return $code;
          }

          return (string)($pfCode) . (string)($code);
     }

     /**
     * 获取错误码前缀注释
     * @return string|null
     * @throws \ReflectionException
     */
     public function getPrefixMsg(): ?string
     {
         return $this->getEnumCodePrefix()?->pfMsg;
     }


    /**
     * 获取错误码前缀
     * @return int|null
     * @throws \ReflectionException
     */
     public function getPrefixCode(): ?int
     {
         return $this->getEnumCodePrefix()?->pfCode;
     }

}