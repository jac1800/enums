<?php
/**
 * @Author: Jackywu
 * @Email: 403226004@qq.com
 * @Time: 2023/12/11 23:29
 */

namespace Jac1800\Enums\Annotations;

use Attribute;

#[Attribute(Attribute::TARGET_CLASS)]
class EnumCodePrefix
{
    public function __construct(
        public readonly int|string|null $pfCode = null,
        public readonly ?string         $pfMsg = null
    )
    {
    }
}