<?php
/**
 * @Author: Jackywu
 * @Email: 403226004@qq.com
 * @Time: 2023/12/11 23:28
 */

namespace Jac1800\Enums\Annotations;

use Attribute;
#[Attribute(Attribute::TARGET_CLASS_CONSTANT)]
class EnumCode
{
    public function __construct(
        public readonly ?string $msg = null,
        public readonly ?array  $ext = null,
    )
    {
    }
}