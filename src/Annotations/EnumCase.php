<?php
/**
 * @Author: Jackywu
 * @Email: 403226004@qq.com
 * @Time: 2023/12/11 23:26
 */

namespace Jac1800\Enums\Annotations;

use Attribute;
#[Attribute(Attribute::TARGET_CLASS_CONSTANT)]
class EnumCase
{
    public function __construct(
        public readonly ?string               $msg = null,
        public readonly mixed                 $name = null,
        public readonly null|string|int|array $group = null,
        public readonly ?array                $ext = null,
    )
    {
    }
}